/**
 * This TestNG class validates functional features of the Spotify Client on
 * the Windows platform through Sikuli API. This test class can also be run 
 * through spotifyDemo.xml
 * PreCondition: Spotify client should be downloaded as an icon on the desktop. The script
 * assumes that the email/username field is populated with 'vasudevanv@aol.com'
 * @author pvasudevan
 */

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import static org.testng.Assert.*;

import org.testng.annotations.Test;


public class SpotifyTest {
Screen s = new Screen();
Images m = new Images();



@BeforeClass
public void openSpotify() throws Exception{
	s.find(m.spotifyIcon);
	s.doubleClick(m.spotifyIcon);
	Thread.sleep(1000);
}


@DataProvider
public Object[][] invalidLoginTestData() {
    return new Object[][]{
        {SpotifyParameters.invalidEmail, SpotifyParameters.invalidPassword},
    };
}

@DataProvider
public Object[][] loginTestData() {
    return new Object[][]{
        {SpotifyParameters.email, SpotifyParameters.password},
    };
}

@DataProvider
public Object[][] playTrackData() {
    return new Object[][]{
        {SpotifyParameters.searchRequest},
    };
}

@DataProvider
public Object[][] createRadioData() {
    return new Object[][]{
        {SpotifyParameters.searchRadio},
    };
}


@Test(dataProvider = "invalidLoginTestData")
public void testInvalidLogin(String invalidEmail, String invalidPassword) throws Exception{
	
	
	if(s.exists(m.username2)!=null){//if username field is prepopulated
		s.click(m.username2);
		m.clear();
	}
	else{ //if username field is blank
		//s.find(m.username);
		s.click(m.username);
	}
	
	s.type(invalidEmail);
	
	//s.find(m.password);
	s.click(m.password);
	s.type(invalidPassword);
	s.find(m.login);
	s.click(m.login);
	Thread.sleep(5000);
	assertTrue(s.exists(m.loginError)!=null);
	
}

@Test(dependsOnMethods="testInvalidLogin", dataProvider = "loginTestData")
public void testLogin(String email, String password) throws Exception{
	s.find(m.usernameError);
	s.click(m.usernameError);
	m.clear();
	
	s.type(email);
	s.type(Key.TAB);
	s.type(Key.BACKSPACE);
	s.find(m.password);
	s.click(m.password);
	s.type(password);
	s.find(m.login);
	s.click(m.login);
	Thread.sleep(3000);
	assertTrue(s.exists(m.spotifyFree)!=null);
	
}

@Test(dependsOnMethods="testLogin")
public void testSearchTrack() throws Exception{
	HashMap<String,String> searchMap = SpotifyParameters.getSearches();
	
	for(String key: searchMap.keySet()){
	s.find(m.search);
	s.click(m.search);
	s.type(key);
	s.type(Key.ENTER);
	
	Thread.sleep(1000);
	assertTrue(s.exists(searchMap.get(key))!=null);
	s.find(m.closeSearch);
	s.click(m.closeSearch);
	}
}

@Test(dependsOnMethods="testSearchTrack", dataProvider = "playTrackData")
public void testPlayTrack(String searchRequest) throws Exception{
	//Play the music
	s.find(m.search);
	s.click(m.search);
	s.type(searchRequest);
	s.find(m.rihannaMusic);
	s.click(m.rihannaMusic);
	Thread.sleep(1000);
	//Pause music
	s.find(m.pause);
	s.click(m.pause);
	
}

@Test(dependsOnMethods="testPlayTrack", dataProvider = "createRadioData")
public void testCreateRadio(String radioStation) throws Exception{
	s.find(m.radio);
	s.click(m.radio);
	s.find(m.createStation);
	s.click();
	s.type(radioStation);
	s.click(m.taylorSwift);
	s.find(m.radioplay);
	s.click(m.radioplay);
	s.click(m.pause);
	
	assertTrue(s.exists(m.taylorswiftradio)!=null);
}

@AfterClass
public void closeSpotify() throws Exception{
	s.find(m.menu);
	s.click(m.menu);
	s.find(m.logOut);
	s.click(m.logOut);
	s.find(m.closeSpotify);
	s.click(m.closeSpotify);
	
}


}

